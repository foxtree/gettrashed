class Enemy:
    def __init__(self, id, enemyType, enemyImg, enemyX, enemyY, enemyChangeX):
        self.enemyId = id
        self.enemyType = enemyType
        self.enemyImg = enemyImg
        self.enemyX = enemyX
        self.enemyY = enemyY
        self.enemyChangeX = enemyChangeX