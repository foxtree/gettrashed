## Description
This project was created in the course of a diploma thesis. The project deals with the topic of recycling and has the goal to convey this important topic in an interesting way. Therefore a game was developed, which  
motivate the players to deal with the topic of recycling and to point out the importance of waste separation.  
the importance of waste separation and disposal.

 

The aim of the game is to use an avatar to dispose of waste in the correct waste containers and thus score as many points as possible.

 


## Note
The project is currently under development, i.e. more features will follow in the future! 


 

## Requirements

 

The following tools or libraries are required to run the application. The versions in brackets indicate the versions used at the time of creation.

 

- Python (Version: 3.11.2) 
- Link to the Tool: https://www.python.org/downloads/
- Pygame (Version: 2.2.0) 
- Link to the library: https://www.pygame.org/news

 

The library can be installed with the following command in the command line:
```
pip install pygame
```

 

## Start application
After cloning the repository, the following command on the command line can be used to start the application.
```
python <your_path>/game.py
```

 

After that, the application should be running or ready to play.
**Hint**: If the application does not start, please check the path to the game or the path to load the images.

 

---

 

## Beschreibung
Dieses Projekt entstand im Zuge einer Diplomarbeit. Das Projekt beschäftigt sich mit dem Thema Recycling und hat das Ziel, dieses wichtige  Thema auf eine interessante Art und Weise zu vermitteln. Dazu wurde ein Spiel entwickelt, dass die  
Spieler:innen dazu motivieren soll, sich mit dem Thema Recycling auseinanderzusetzen und damit  
auf die Wichtigkeit von Abfalltrennung und -entsorgung aufmerksam zu machen.

 

Ziel des Spiel ist es, mit einem Avatar, Abfall in die richtigen Abfallcontainer zu entsorgen und somit möglichst viele Spielpunkte zu erreichen.

 


## Hinweis
Das Projekt wird derzeit noch weiterentwickelt, d.h. es folgen weitere Features in Zukunft! 

## Voraussetzungen

 

Für das Starten der Anwendung sind folgende Tools bzw. Bibliotheken notwendig. Die in den Klammern stehenden Versionen, geben die zur Zeit der Erstellung verwendeten Versionen an.

 

- Python (Version: 3.11.2) 
- Link zum Tool: https://www.python.org/downloads/
- Pygame (Version: 2.2.0) 
- Link zur Bibliothek: https://www.pygame.org/news

 

Die Bibliothek kann mit dem folgenden Befehl in der Kommandozeile installiert werden:
```
pip install pygame
```

 

## Anwendung starten
Nach dem Klonen des Repository kann mit folgenden Befehl in der Kommandozeile die Anwendung gestartet werden.
```
python <your_path>/game.py
```
Danach sollte die Anwendung laufen bzw. spielbereit sein.
**Tipp**: Sollte die Anwendung nicht starten, dann kontrollieren Sie Bitte den Pfad zum Spiel, bzw. jenen zum Laden der Bilder.
