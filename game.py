import pygame
import math
import random
import json

from classes.enemy import Enemy

# Intialize The pygame
pygame.init()

# Create the screen
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 800

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

# Background
background = pygame.transform.scale(pygame.image.load(
    'img/background.png'), (SCREEN_WIDTH, SCREEN_HEIGHT))

# Title and Icon
pygame.display.set_caption("Get Trashed")
icon = pygame.image.load('img/Icon.png')
pygame.display.set_icon(icon)

# Scaling
SCALE = SCREEN_WIDTH/8

# menu IMG
menuScaleX = 200
menuScaleY = 80
startImg = icon = pygame.transform.scale(
    pygame.image.load('img/start.png'), (menuScaleX, menuScaleY))
settingsImg = icon = pygame.transform.scale(
    pygame.image.load('img/settings.png'), (menuScaleX, menuScaleY))
menuScoreImg = icon = pygame.transform.scale(
    pygame.image.load('img/score.png'), (menuScaleX, menuScaleY))
creditsImg = icon = pygame.transform.scale(
    pygame.image.load('img/credits.png'), (menuScaleX, menuScaleY))
exitImg = icon = pygame.transform.scale(
    pygame.image.load('img/exit.png'), (menuScaleX, menuScaleY))
startHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/startHighlight.png'), (menuScaleX, menuScaleY))
settingsHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/settingsHighlight.png'), (menuScaleX, menuScaleY))
scoreHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/scoreHighlight.png'), (menuScaleX, menuScaleY))
creditsHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/creditsHighlight.png'), (menuScaleX, menuScaleY))
exitHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/exitHighlight.png'), (menuScaleX, menuScaleY))

ViewTop10Img = icon = pygame.transform.scale(
    pygame.image.load('img/ViewTop10.png'), (menuScaleX, menuScaleY))
ViewTop10HighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/ViewTop10Highlight.png'), (menuScaleX, menuScaleY))

BackToMenuImg = icon = pygame.transform.scale(
    pygame.image.load('img/BackToMenu.png'), (menuScaleX, menuScaleY))
BackToMenuHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/BackToMenuHighlight.png'), (menuScaleX, menuScaleY))

ResumeImg = icon = pygame.transform.scale(
    pygame.image.load('img/Resume.png'), (menuScaleX, menuScaleY))
ResumeHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('img/ResumeHighlight.png'), (menuScaleX, menuScaleY))

# Pause images
pauseImages = [[], [], [], [], []]
pauseImages[0].append(pygame.transform.scale(pygame.image.load('Facts/Fact1.png'), (SCREEN_WIDTH - 100, 300)))
pauseImages[1].append(pygame.transform.scale(pygame.image.load('Facts/Fact2.png'), (SCREEN_WIDTH - 100, 300)))
pauseImages[2].append(pygame.transform.scale(pygame.image.load('Facts/Fact3.png'), (SCREEN_WIDTH - 100, 300)))
pauseImages[3].append(pygame.transform.scale(pygame.image.load('Facts/Fact4.png'), (SCREEN_WIDTH - 100, 300)))
pauseImages[4].append(pygame.transform.scale(pygame.image.load('Facts/Fact5.png'), (SCREEN_WIDTH - 100, 300)))
pauseFact = 1


startX = (SCREEN_WIDTH / 2) - (menuScaleX / 2)
startY = (SCREEN_HEIGHT / 2) - (menuScaleY + 250)

settingsX = (SCREEN_WIDTH / 2) - (menuScaleX / 2)
settingsY = (SCREEN_HEIGHT / 2) - (menuScaleY + 100)

menuScoreX = (SCREEN_WIDTH / 2) - (menuScaleX / 2)
menuScoreY = (SCREEN_HEIGHT / 2) - (menuScaleY / 2)

creditsX = (SCREEN_WIDTH / 2) - (menuScaleX / 2)
creditsY = (SCREEN_HEIGHT / 2) - (menuScaleY - 200)

exitX = (SCREEN_WIDTH / 2) - (menuScaleX / 2)
exitY = (SCREEN_HEIGHT / 2) - (menuScaleY - 350)


def drawImg(x, y, Img):
    screen.blit(Img, (x, y))


# Score
scoreValue = 0

scoreX = 10
scoreY = 25


def showScore(x, y):
    font = pygame.font.Font('Font/joystix_monospace.ttf', 20)
    score = font.render("Score : " + str(scoreValue), True, (0, 0, 0))
    screen.blit(score, (scoreX, scoreY))


# EXP
EXPImg = icon = pygame.image.load('img/EXP.png')
EXPStart = -1990
ExpX = EXPStart
ExpY = -25
ExpKill = SCREEN_WIDTH / 5
ExpCoin = SCREEN_WIDTH / 20

# LvL Up IMGs
LvLUpImgScale = SCALE / 2
# BulletSpeed increase
bulletSpeedImg = icon = pygame.transform.scale(pygame.image.load(
    'img/bulletSpeed.png'), (LvLUpImgScale, LvLUpImgScale))
bulletSpeedImgX = (SCREEN_WIDTH / 2) - (LvLUpImgScale / 2) - 150
bulletSpeedImgY = (SCREEN_HEIGHT / 2) - (LvLUpImgScale / 2)

# EnemySlow increase
enemySlowImg = icon = pygame.transform.scale(
    pygame.image.load('img/Slow.png'), (LvLUpImgScale, LvLUpImgScale))
enemySlowX = bulletSpeedImgX
enemySlowY = (SCREEN_HEIGHT / 2) - (LvLUpImgScale / 2) + 100

# PlayerHp increase
Hp = 1
HpLvLX = enemySlowX
HpLvLY = SCREEN_HEIGHT / 2 - (LvLUpImgScale / 2) - 100


def HPLvL(x, y):
    font = pygame.font.Font('Font/joystix_monospace.ttf', 15)
    screen.blit(HPImgLvL, (x, y))
    HPLvL = font.render(str(Hp), True, (0, 0, 0))
    screen.blit(HPLvL, (HpLvLX + 10, HpLvLY + 5))


HPImgLvL = icon = pygame.transform.scale(pygame.image.load(
    'img/herz.png'), (LvLUpImgScale, LvLUpImgScale))

# HP - Health Points
HpValue = 5
HpX = SCREEN_WIDTH - 50
HpY = SCREEN_HEIGHT - 40
HpScale = SCALE / 2
HPImg = icon = pygame.transform.scale(
    pygame.image.load('img/herz.png'), (HpScale, HpScale))
HPImgX = SCREEN_WIDTH - 60
HPImgY = HpY - 5


def showHP(x, y):
    font = pygame.font.Font('Font/joystix_monospace.ttf', 15)
    screen.blit(HPImg, (HPImgX, HPImgY))
    Health = font.render(str(HpValue), True, (0, 0, 0))
    screen.blit(Health, (HpX, HpY))


# Player
playerScale = SCALE
playerImg = icon = pygame.transform.scale(
    pygame.image.load('img/cannon.png'), (playerScale, playerScale))

# Player Position
playerX = (SCREEN_WIDTH / 2) - (SCALE / 2)
playerY = SCREEN_HEIGHT - 175
playerChangeX = 0
playerSpeed = 8


def collision(X, Y, X2, Y2, ScaleX, ScaleY, ScaleX2, ScaleY2, HitboxDistance):
    distance = math.sqrt((math.pow((X + ScaleX / 2) - (X2 + ScaleX2 / 2), 2)) +
                         (math.pow((Y + ScaleY / 2) - (Y2 + ScaleY2 / 2), 2)))
    if distance < HitboxDistance / 2:
        return True
    else:
        return False

# ------------------------------------------------------------------------------------------------------------------------------------------------
# COINS


# More coins
coinImg = []
coinX = []
coinY = []
coinChangeY = []
coinSpeed = []

# nummber of the coins
numOfCoins = 1
numOfCoinsMax = 5
coinsSpawnA = SCREEN_HEIGHT / 40
coinsSpawnB = SCREEN_HEIGHT / 20
coinsScaling = SCALE / 2

for i in range(numOfCoinsMax):
    if numOfCoins <= numOfCoinsMax:

        coinImg.append(pygame.transform.scale(pygame.image.load(
            'img/coin.png'), (coinsScaling, coinsScaling)))

        coinX.append(random.randint(0, SCREEN_WIDTH - int(coinsScaling)))
        coinY.append(random.randint(coinsSpawnA, coinsSpawnB))
        coinSpeed.append(random.randint((playerSpeed / 2), playerSpeed))

        coinChangeY.append(coinSpeed[i])

# ------------------------------------------------------------------------------------------------------------------------------------------------
# ENEMIES
# ------------------------------------------------------------------------------------------------------------------------------------------------
enemySpawnA = 100
enemySpawnB = 200
enemyScaling = SCALE * (10 / 9)
enemySpeed = playerSpeed / 2
enemyCanImg = pygame.transform.scale(pygame.image.load('img/enemyCan.png'), (enemyScaling, enemyScaling))
enemyBottleImg = pygame.transform.scale(pygame.image.load('img/enemyBottle.png'), (enemyScaling, enemyScaling))
enemyPaperImg = pygame.transform.scale(pygame.image.load('img/enemyPaper.png'), (enemyScaling, enemyScaling))

def movingImg(x, y, i, movingImgType):
    screen.blit(movingImgType[i], (x, y))


def movingEnemyImg(enemy: Enemy):
    screen.blit(enemy.enemyImg, (enemy.enemyX, enemy.enemyY))


def createEnemy(id, typ, img):
    # Speed
    randomDirection = random.randint(-1, 1)
    if randomDirection < 0:
        randomDirection = -1
    else:
        randomDirection = 1
    return Enemy(id, typ, img, random.randint(0, SCREEN_WIDTH - int(enemyScaling)), random.randint(enemySpawnA, enemySpawnB), enemySpeed*randomDirection)

# Enemies
numOfEnemies = 4
enemies = []
for i in range(numOfEnemies):
    RadomEnemyInt = random.randint(0,3)
    if RadomEnemyInt < 1:
        enemies.append(createEnemy(i, "CAN", enemyCanImg))
    elif RadomEnemyInt > 2:
        enemies.append(createEnemy(i, "BOTTLE", enemyBottleImg))
    else:
        enemies.append(createEnemy(i, "PAPER", enemyPaperImg))


# ------------------------------------------------------------------------------------------------------------------------------------------------

bulletScale = SCALE / 2

nextBulletX = SCREEN_WIDTH - (SCREEN_WIDTH - 50 + (bulletScale))
nextBulletY = SCREEN_HEIGHT - 40

# Bullet
bulletState = "ready"

canImg = icon = pygame.transform.scale(
    pygame.image.load('img/can.png'), (bulletScale, bulletScale))
bottleImg = icon = pygame.transform.scale(
    pygame.image.load('img/bottle.png'), (bulletScale, bulletScale))
paperImg = icon = pygame.transform.scale(
    pygame.image.load('img/paper.png'), (bulletScale, bulletScale))
bulletRandom = 0

# Bullet icon


def fireBullet(x, y):
    global bulletState
    bulletState = "fire"
    if bulletRandom < 1:
        screen.blit(canImg, (x + 16, y + 10))
    elif bulletRandom > 2:
        screen.blit(bottleImg, (x + 16, y + 10))
    else:
        screen.blit(paperImg, (x + 16, y + 10))


# Bullet Position
# Ready - You can't see the bullet
# Fire - Bullet is moving
bulletX = 0
bulletY = SCREEN_HEIGHT - 165

# Speed of the bullet
bulletSpeed = 24
bulletChangeY = bulletSpeed
bulletSpeedModifier = 0.1

bulletNew = 1

# ------------------------------------------------------------------------------------------------------------------------------------------------

# FPS = frames per second is the speed of the game
Fps = 60
clock = pygame.time.Clock()

LvLCounter = 0

timerTick = 0
timerSeconds = 0
timerMinutes = 2
font = pygame.font.Font('Font/joystix_monospace.ttf', 20)
timerX = SCREEN_WIDTH - 90
timerY = 25


def showtimer(x, y):
    timer = font.render(str(timerMinutes).zfill(2) + ":" +
                        str(timerSeconds).zfill(2), True, (0, 0, 0))
    screen.blit(timer, (x, y))


def load_highscores():
    try:
        with open("highscores.json") as json_file:
            return json.load(json_file)
    except:
        return []


def save_highscores(highscores):
    with open("highscores.json", "w") as json_file:
        json.dump(highscores, json_file)


def add_highscore(name, score):
    if score != 0:
        highscores = load_highscores()
        highscores.append({"name": name, "score": score})
        highscores.sort(key=lambda x: x["score"], reverse=True)
        save_highscores(highscores)


def display_highscores():
    highscores = load_highscores()
    y = 100
    rank = 1
    font = pygame.font.Font('Font/joystix_monospace.ttf', 25)
    first = font.render("Rank", True, (0, 0, 0))
    second = font.render("Name", True, (0, 0, 0))
    third = font.render("Score", True, (0, 0, 0))
    screen.blit(first, ((SCREEN_WIDTH / 2) - 200, y))
    screen.blit(second, ((SCREEN_WIDTH / 2) - 40, y))
    screen.blit(third, ((SCREEN_WIDTH / 2) + 110, y))
    for i, highscore in enumerate(highscores):
        if i < 10:
            currentHighscoreName = highscore['name'][:10]
            currentHighscore = highscore['score']
            if currentHighscoreName != "":
                y += 35
                first = font.render(str(rank) + ".", True, (0, 0, 0))
                second = font.render(
                    str(currentHighscoreName), True, (0, 0, 0))
                third = font.render(str(currentHighscore), True, (0, 0, 0))
                screen.blit(first, ((SCREEN_WIDTH / 2) - 170, y))
                screen.blit(second, ((SCREEN_WIDTH / 2) - 120, y))
                screen.blit(third, ((SCREEN_WIDTH / 2) + 150, y))
                pygame.draw.line(screen, (0, 0, 0), (200, y - 5),
                                 (200, y - 3), SCREEN_HEIGHT)
                rank += 1


def showHeadline(x, y, name, scale):
    font = pygame.font.Font('Font/joystix_monospace.ttf', scale)
    fullName = font.render(str(name), True, (0, 0, 0))
    screen.blit(fullName, (x, y))


# Run
runningMain = True
runningGame = False
runningGamePause = False
runningMenu = True
runningLvL = False
runningTop10 = False
runningSettings = False
runningcredits = False
runningGameOver = False
runningHighscore = False

menuCounter = 0
menuCounterChange = 1

canX = 0
canY = 0
bottleX = 0
bottleY = 0
paperX = 0
paperY = 0

keyScaleXY = 40
row1 = (SCREEN_HEIGHT / 2) - 100
row2 = row1 + keyScaleXY
row3 = row2 + keyScaleXY
column1 = (SCREEN_WIDTH - (keyScaleXY * 10)) / 2
column2 = column1 + keyScaleXY
column3 = column2 + keyScaleXY
column4 = column3 + keyScaleXY
column5 = column4 + keyScaleXY
column6 = column5 + keyScaleXY
column7 = column6 + keyScaleXY
column8 = column7 + keyScaleXY
column9 = column8 + keyScaleXY
column10 = column9 + keyScaleXY

qImg = icon = pygame.transform.scale(
    pygame.image.load('key/Q.png'), (keyScaleXY, keyScaleXY))
wImg = icon = pygame.transform.scale(
    pygame.image.load('key/W.png'), (keyScaleXY, keyScaleXY))
eImg = icon = pygame.transform.scale(
    pygame.image.load('key/E.png'), (keyScaleXY, keyScaleXY))
rImg = icon = pygame.transform.scale(
    pygame.image.load('key/R.png'), (keyScaleXY, keyScaleXY))
tImg = icon = pygame.transform.scale(
    pygame.image.load('key/T.png'), (keyScaleXY, keyScaleXY))
zImg = icon = pygame.transform.scale(
    pygame.image.load('key/Z.png'), (keyScaleXY, keyScaleXY))
uImg = icon = pygame.transform.scale(
    pygame.image.load('key/U.png'), (keyScaleXY, keyScaleXY))
iImg = icon = pygame.transform.scale(
    pygame.image.load('key/I.png'), (keyScaleXY, keyScaleXY))
oImg = icon = pygame.transform.scale(
    pygame.image.load('key/O.png'), (keyScaleXY, keyScaleXY))
pImg = icon = pygame.transform.scale(
    pygame.image.load('key/P.png'), (keyScaleXY, keyScaleXY))
aImg = icon = pygame.transform.scale(
    pygame.image.load('key/A.png'), (keyScaleXY, keyScaleXY))
sImg = icon = pygame.transform.scale(
    pygame.image.load('key/S.png'), (keyScaleXY, keyScaleXY))
dImg = icon = pygame.transform.scale(
    pygame.image.load('key/D.png'), (keyScaleXY, keyScaleXY))
fImg = icon = pygame.transform.scale(
    pygame.image.load('key/F.png'), (keyScaleXY, keyScaleXY))
gImg = icon = pygame.transform.scale(
    pygame.image.load('key/G.png'), (keyScaleXY, keyScaleXY))
hImg = icon = pygame.transform.scale(
    pygame.image.load('key/H.png'), (keyScaleXY, keyScaleXY))
jImg = icon = pygame.transform.scale(
    pygame.image.load('key/J.png'), (keyScaleXY, keyScaleXY))
kImg = icon = pygame.transform.scale(
    pygame.image.load('key/K.png'), (keyScaleXY, keyScaleXY))
lImg = icon = pygame.transform.scale(
    pygame.image.load('key/L.png'), (keyScaleXY, keyScaleXY))
yImg = icon = pygame.transform.scale(
    pygame.image.load('key/Y.png'), (keyScaleXY, keyScaleXY))
xImg = icon = pygame.transform.scale(
    pygame.image.load('key/X.png'), (keyScaleXY, keyScaleXY))
cImg = icon = pygame.transform.scale(
    pygame.image.load('key/C.png'), (keyScaleXY, keyScaleXY))
vImg = icon = pygame.transform.scale(
    pygame.image.load('key/V.png'), (keyScaleXY, keyScaleXY))
bImg = icon = pygame.transform.scale(
    pygame.image.load('key/B.png'), (keyScaleXY, keyScaleXY))
nImg = icon = pygame.transform.scale(
    pygame.image.load('key/N.png'), (keyScaleXY, keyScaleXY))
mImg = icon = pygame.transform.scale(
    pygame.image.load('key/M.png'), (keyScaleXY, keyScaleXY))
spaceImg = icon = pygame.transform.scale(
    pygame.image.load('key/Space.png'), (keyScaleXY * 3, keyScaleXY))
LöschenImg = icon = pygame.transform.scale(
    pygame.image.load('key/Löschen.png'), (keyScaleXY, keyScaleXY))

qHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/Q.png'), (keyScaleXY, keyScaleXY))
wHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/W.png'), (keyScaleXY, keyScaleXY))
eHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/E.png'), (keyScaleXY, keyScaleXY))
rHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/R.png'), (keyScaleXY, keyScaleXY))
tHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/T.png'), (keyScaleXY, keyScaleXY))
zHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/Z.png'), (keyScaleXY, keyScaleXY))
uHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/U.png'), (keyScaleXY, keyScaleXY))
iHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/I.png'), (keyScaleXY, keyScaleXY))
oHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/O.png'), (keyScaleXY, keyScaleXY))
pHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/P.png'), (keyScaleXY, keyScaleXY))
aHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/A.png'), (keyScaleXY, keyScaleXY))
sHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/S.png'), (keyScaleXY, keyScaleXY))
dHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/D.png'), (keyScaleXY, keyScaleXY))
fHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/F.png'), (keyScaleXY, keyScaleXY))
gHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/G.png'), (keyScaleXY, keyScaleXY))
hHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/H.png'), (keyScaleXY, keyScaleXY))
jHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/J.png'), (keyScaleXY, keyScaleXY))
kHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/K.png'), (keyScaleXY, keyScaleXY))
lHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/L.png'), (keyScaleXY, keyScaleXY))
yHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/Y.png'), (keyScaleXY, keyScaleXY))
xHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/X.png'), (keyScaleXY, keyScaleXY))
cHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/C.png'), (keyScaleXY, keyScaleXY))
vHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/V.png'), (keyScaleXY, keyScaleXY))
bHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/B.png'), (keyScaleXY, keyScaleXY))
nHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/N.png'), (keyScaleXY, keyScaleXY))
mHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/M.png'), (keyScaleXY, keyScaleXY))
spaceHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/Space.png'), (keyScaleXY * 3, keyScaleXY))
LöschenHighlightImg = icon = pygame.transform.scale(
    pygame.image.load('key/Highlight/Löschen.png'), (keyScaleXY, keyScaleXY))

keyRowCounter = 1
keyColumnCounter = 1
keyCounterChange = 1
nameAdd = ""
name = ""
highlightX = (SCREEN_WIDTH / 2) - 200
nameY = 150
endScoreY = nameY - 70

def showName(x, y):
    font = pygame.font.Font('Font/joystix_monospace.ttf', 30)
    FullName = font.render("Name : " + str(name)[:10], True, (0, 0, 0))
    screen.blit(FullName, (x, y))

def showEndScore(x, y):
    font = pygame.font.Font('Font/joystix_monospace.ttf', 30)
    endScore = font.render("Score : " + str(scoreValue), True, (0, 0, 0))
    screen.blit(endScore, (x, y))

while runningMain:
    clock.tick(10)
    while runningMenu:

        resetscoreValue = 0
        scoreValue = resetscoreValue
        resetExpX = -1990
        ExpX = resetExpX
        resetHpValue = 5
        HpValue = resetHpValue
        resetnumOfCoins = 1
        numOfCoins = resetnumOfCoins
        resetenemySpeed = playerSpeed / 2
        enemySpeed = resetenemySpeed
        resettimerSeconds = 0
        timerSeconds = resettimerSeconds
        resettimerMinutes = 2
        timerMinutes = resettimerMinutes
        resetname = ""
        name = resetname
        resetbulletX = 0
        bulletX = resetbulletX
        resetplayerX = (SCREEN_WIDTH / 2) - (SCALE / 2)
        playerX = resetplayerX

        clock.tick(Fps)
        screen.fill((222, 208, 221))
        drawImg(startX, startY, startImg)
        drawImg(settingsX, settingsY, settingsImg)
        drawImg(menuScoreX, menuScoreY, menuScoreImg)
        drawImg(creditsX, creditsY, creditsImg)
        drawImg(exitX, exitY, exitImg)
        if menuCounter == 0:
            drawImg(startX, startY, startHighlightImg)
        if menuCounter == 1:
            drawImg(settingsX, settingsY, settingsHighlightImg)
        if menuCounter == 2:
            drawImg(menuScoreX, menuScoreY, scoreHighlightImg)
        if menuCounter == 3:
            drawImg(creditsX, creditsY, creditsHighlightImg)
        if menuCounter == 4:
            drawImg(exitX, exitY, exitHighlightImg)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                runningMenu = False                
                runningMain = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and menuCounter == 0:
                    runningMenu = False
                    runningGame = True
                if event.key == pygame.K_SPACE and menuCounter == 2:
                    runningMenu = False
                    runningHighscore = True
                if event.key == pygame.K_SPACE and menuCounter == 4:
                    runningMenu = False
                    runningMain = False
                if event.key == pygame.K_UP:
                    menuCounter -= menuCounterChange
                if event.key == pygame.K_DOWN:
                    menuCounter += menuCounterChange

        if menuCounter == - 1:
            menuCounter = 0
        if menuCounter == 5:
            menuCounter = 4
        pygame.display.update()

    # Game Loop
    while runningGame:

        clock.tick(Fps)
        timerTick += 1
        if timerTick == 60:
            timerTick = 0
            timerSeconds -= 1
            print(str(timerMinutes).zfill(2) + ":" + str(timerSeconds).zfill(2))
        if timerSeconds == 0:
            timerSeconds = 60
            timerMinutes -= 1
        if timerMinutes == -1:
            runningGame = False
            runningGameOver = True
        if bulletRandom < 1:
            canX = bulletX
            canY = bulletY
        elif bulletRandom > 2:
            bottleX = bulletX
            bottleY = bulletY
        else:
            paperX = bulletX
            paperY = bulletY
        # Background Image
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                runningGame = False
                runningMain = False

            # When a key is pressed, check if it is left or right
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    runningGame = False

                if event.key == pygame.K_LEFT:
                    # Moving speed of the player (Left)
                    playerChangeX = - playerSpeed

                if event.key == pygame.K_RIGHT:
                    # Moving speed of the player (right)
                    playerChangeX = playerSpeed

                if event.key == pygame.K_p:
                    runningGamePause = True
                    runningGame= False

                # Bullet shooting
                if event.key == pygame.K_SPACE:
                    if bulletState == "ready":
                        bulletX = playerX
                        fireBullet(bulletX, bulletY)

                if event.key == pygame.K_r:
                    bulletRandomNew = random.randint(0, 3)
                    bulletRandom = bulletRandomNew
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    playerChangeX = 0

        # Player movement
        playerX += playerChangeX

        # Player stays in the screen cant move out
        if playerX <= 0:
            playerX = 0
        elif playerX >= SCREEN_WIDTH - SCALE:
            playerX = SCREEN_WIDTH - SCALE
        
        # Enemy movement
        for i in range(len(enemies)-1):
            enemies[i].enemyX += enemies[i].enemyChangeX

            # Enemy stays in the screen cant move out
            if enemies[i].enemyX <= 0:
                # Moving speed of the enemy after touching the boarder (right)
                enemies[i].enemyChangeX = enemySpeed
            elif enemies[i].enemyX >= SCREEN_WIDTH - int(enemyScaling):
                # Moving speed of the enemy after touching the boarder (left)
                enemies[i].enemyChangeX = - enemySpeed

            # Collision
            isCollision = collision(enemies[i].enemyX, enemies[i].enemyY, bulletX, bulletY, enemyScaling, enemyScaling, bulletScale, bulletScale, enemyScaling)
            if isCollision:
                print("collision")
                bulletY = SCREEN_HEIGHT - 100
                bulletState = "ready"
                scoreValue += 1
                ExpX += ExpKill
                print("Score: " + str(scoreValue))

                # delete enemy from list
                print(str(enemies[i].enemyType) + " was killed")
                enemies.remove(enemies[i])

                # relaod 
                movingEnemyImg(enemies[i])

                RadomEnemyInt = random.randint(0, 9)
                if RadomEnemyInt < 3:
                    #create new can enemy
                    enemies.append(createEnemy(i, "CAN", enemyCanImg))
                elif RadomEnemyInt > 6:
                    #create new bottle enemy
                    enemies.append(createEnemy(i, "BOTTLE", enemyBottleImg))
                else:
                    #create new paper enemy
                    enemies.append(createEnemy(i, "PAPER", enemyPaperImg))
                   
            # Enemy Coordination
            movingEnemyImg(enemies[i])

        # Coins rain from the sky
        for i in range(numOfCoins):
            coinY[i] += coinChangeY[i]
            collisionCoin = collision(coinX[i], coinY[i], playerX, playerY,
                                    coinsScaling, coinsScaling, playerScale, playerScale, playerScale)
            if collisionCoin:
                ExpX += ExpCoin
                print("Coin collected")
                coinX[i] = random.randint(0, SCREEN_WIDTH - int(coinsScaling))
                coinY[i] = random.randint(coinsSpawnA, coinsSpawnB)
            if coinY[i] >= SCREEN_HEIGHT:
                coinX[i] = random.randint(0, SCREEN_WIDTH - int(coinsScaling))
                coinY[i] = random.randint(coinsSpawnA, coinsSpawnB)
            movingImg(coinX[i], coinY[i], i, coinImg)

        # Counting the LvL ups
        if LvLCounter == 5:
            LvLCounter = 0
            if numOfCoins < numOfCoinsMax:
                numOfCoins = numOfCoins + 1

        if bulletState == "ready":
            if bulletRandom < 1:
                drawImg(nextBulletX, nextBulletY, canImg)
            elif bulletRandom > 2:
                drawImg(nextBulletX, nextBulletY, bottleImg)
            else:
                drawImg(nextBulletX, nextBulletY, paperImg)
            while bulletNew == 1:
                bulletRandomNew = random.randint(0, 3)
                bulletRandom = bulletRandomNew
                bulletRandomNew = 10
                print("bullet:" + str(bulletRandom))
                bulletNew = 0

        # Bullet Movement
        if bulletY <= 0:
            bulletY = SCREEN_HEIGHT - 165
            paperY = bulletY
            canY = bulletY
            bottleY = bulletY
            bulletState = "ready"
            HpValue -= 1

        if bulletState == "fire":
            fireBullet(bulletX, bulletY)
            bulletY -= bulletChangeY
            bulletNew = 1

        # HP = 0 Lose game
        if HpValue == 0:
            runningGameOver = True
            runningGame = False

        # EXP = Full = LvL up
        if ExpX >= EXPStart + SCREEN_WIDTH:
            runningLvL = True
            runningGame = False
            ExpX = EXPStart
            LvLCounter = LvLCounter + 1
            print("Counter: " + str(LvLCounter))
    
        # Player Coordination
        drawImg(playerX, playerY, playerImg)

        # Expbar displays
        drawImg(ExpX, ExpY, EXPImg)

        # Score displays
        showScore(scoreX, scoreY)
        showtimer(timerX, timerY)

        # HP displays
        showHP(HpX, HpY)
        pygame.draw.line(screen, (0, 0, 0), (200, 22),
                                 (200, 24), SCREEN_HEIGHT)
        pygame.display.update()
    # LvL up Loop
    while runningLvL:

        clock.tick(Fps)
        screen.fill((222, 208, 221))
        showHeadline(bulletSpeedImgX + 50, HpLvLY + 5, "Health Up", 15)
        showHeadline(bulletSpeedImgX + 50, bulletSpeedImgY + 5, "Projektile speed increase", 15)
        showHeadline(bulletSpeedImgX + 50, enemySlowY + 5, "Enemy speed decrease", 15)
        HPLvL(HpLvLX, HpLvLY)
        drawImg(bulletSpeedImgX, bulletSpeedImgY, bulletSpeedImg)
        drawImg(enemySlowX, enemySlowY, enemySlowImg)
        if keyRowCounter == 1:
            pygame.draw.rect(screen, (0, 0, 0), (bulletSpeedImgX - 30, HpLvLY - 10, 400, 50), 5)
        if keyRowCounter == 2:
            pygame.draw.rect(screen, (0, 0, 0), (bulletSpeedImgX - 30, bulletSpeedImgY - 10, 400, 50), 5)
        if keyRowCounter == 3:
            pygame.draw.rect(screen, (0, 0, 0), (bulletSpeedImgX - 30, enemySlowY - 10, 400, 50), 5)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                runningLvL = False
                runningMain = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    runningGame = False
                    runningLvL = False

                if event.key == pygame.K_UP:
                    keyRowCounter -= keyCounterChange
                if event.key == pygame.K_DOWN:
                    keyRowCounter += keyCounterChange

                # Press a to LvL up your health points ( +1 )
                if event.key == pygame.K_SPACE and keyRowCounter == 1:
                    HpValue += Hp
                    runningLvL = False
                    runningGame = True
                    print("Health: " + str(HpValue))

                # Press s to LvL up your bullet speed by a modifier of 10% every LvL up in bulletspeed is decreast by 0,5% ( 10% - 0,5% )
                if event.key == pygame.K_SPACE and keyRowCounter == 2:
                    bulletChangeY = round(
                        bulletChangeY + bulletChangeY * bulletSpeedModifier, 2)
                    if bulletSpeedModifier >= 0.01:
                        bulletSpeedModifier = round(
                            bulletSpeedModifier - 0.005, 3)

                    print("Bullet Speed: " + str(bulletChangeY))
                    print("Bullet Modifier: " + str(bulletSpeedModifier))
                    runningLvL = False
                    runningGame = True

                # Press d to LvL up enemslow by a modifier of 25% every LvL up in enemy slow is decreast by 25%
                if event.key == pygame.K_SPACE and keyRowCounter == 3:
                    enemySpeedLvL = enemySpeed / 25
                    enemySpeed = enemySpeed - enemySpeedLvL
                    print("Enemy Speed: " + str(enemySpeed))
                    runningLvL = False
                    runningGame = True
        if keyRowCounter > 4:
            keyRowCounter = 1                                                               
        if keyRowCounter == 0:
            keyRowCounter = 1
        if keyRowCounter == 4:
            keyRowCounter = 3
        pygame.display.update()

    while runningGamePause:
        clock.tick(Fps)
        screen.fill((222, 208, 221))
        showHeadline((SCREEN_WIDTH / 2) - 170, 30, "Game Paused", 37)

        # show fact
        if pauseFact == 1:
            fact = pauseImages[random.randint(0, 4)][0]
            pauseFact = 0
        drawImg(50, 150, fact)
        drawImg(creditsX, creditsY, ResumeImg)
        drawImg(exitX, exitY, BackToMenuImg)
        if keyRowCounter == 1:
            drawImg(creditsX, creditsY, ResumeHighlightImg)
        if keyRowCounter == 2:
            drawImg(exitX, exitY, BackToMenuHighlightImg)        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                runningGamePause = False
                runningMain = False 
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    keyRowCounter -= keyCounterChange
                if event.key == pygame.K_DOWN:
                    keyRowCounter += keyCounterChange
                if event.key == pygame.K_SPACE and keyRowCounter == 1:
                    pauseFact = 1
                    runningGame = True
                    runningGamePause = False
                if event.key == pygame.K_SPACE and keyRowCounter == 2:
                    pauseFact = 1
                    runningMenu = True
                    runningGamePause = False 
        if keyRowCounter > 3:
            keyRowCounter = 1                                                               
        if keyRowCounter == 0:
            keyRowCounter = 1
        if keyRowCounter == 3:
            keyRowCounter = 2
        pygame.display.update()

    while runningGameOver:

        clock.tick(Fps)
        screen.fill((222, 208, 221))
        showName(highlightX, nameY)
        showEndScore(highlightX, endScoreY)

        drawImg(creditsX, creditsY, ViewTop10Img)
        drawImg(exitX, exitY, BackToMenuImg)

        drawImg(column1, row1, qImg)
        drawImg(column2, row1, wImg)
        drawImg(column3, row1, eImg)
        drawImg(column4, row1, rImg)
        drawImg(column5, row1, tImg)
        drawImg(column6, row1, zImg)
        drawImg(column7, row1, uImg)
        drawImg(column8, row1, iImg)
        drawImg(column9, row1, oImg)
        drawImg(column10, row1, LöschenImg)

        drawImg(column1, row2, aImg)
        drawImg(column2, row2, sImg)
        drawImg(column3, row2, dImg)
        drawImg(column4, row2, fImg)
        drawImg(column5, row2, gImg)
        drawImg(column6, row2, hImg)
        drawImg(column7, row2, jImg)
        drawImg(column8, row2, kImg)
        drawImg(column9, row2, lImg)
        drawImg(column10, row2, pImg)

        drawImg(column1, row3, yImg)
        drawImg(column2, row3, xImg)
        drawImg(column3, row3, cImg)
        drawImg(column4, row3, spaceImg)
        drawImg(column7, row3, vImg)
        drawImg(column8, row3, bImg)
        drawImg(column9, row3, nImg)
        drawImg(column10, row3, mImg)

        if keyColumnCounter == 1 and keyRowCounter == 1:
            drawImg(column1, row1, qHighlightImg)
        if keyColumnCounter == 2 and keyRowCounter == 1:
            drawImg(column2, row1, wHighlightImg)
        if keyColumnCounter == 3 and keyRowCounter == 1:
            drawImg(column3, row1, eHighlightImg)
        if keyColumnCounter == 4 and keyRowCounter == 1:
            drawImg(column4, row1, rHighlightImg)
        if keyColumnCounter == 5 and keyRowCounter == 1:
            drawImg(column5, row1, tHighlightImg)
        if keyColumnCounter == 6 and keyRowCounter == 1:
            drawImg(column6, row1, zHighlightImg)
        if keyColumnCounter == 7 and keyRowCounter == 1:
            drawImg(column7, row1, uHighlightImg)
        if keyColumnCounter == 8 and keyRowCounter == 1:
            drawImg(column8, row1, iHighlightImg)
        if keyColumnCounter == 9 and keyRowCounter == 1:
            drawImg(column9, row1, oHighlightImg)
        if keyColumnCounter == 10 and keyRowCounter == 1:
            drawImg(column10, row1, LöschenHighlightImg)

        if keyColumnCounter == 1 and keyRowCounter == 2:
            drawImg(column1, row2, aHighlightImg)
        if keyColumnCounter == 2 and keyRowCounter == 2:
            drawImg(column2, row2, sHighlightImg)
        if keyColumnCounter == 3 and keyRowCounter == 2:
            drawImg(column3, row2, dHighlightImg)
        if keyColumnCounter == 4 and keyRowCounter == 2:
            drawImg(column4, row2, fHighlightImg)
        if keyColumnCounter == 5 and keyRowCounter == 2:
            drawImg(column5, row2, gHighlightImg)
        if keyColumnCounter == 6 and keyRowCounter == 2:
            drawImg(column6, row2, hHighlightImg)
        if keyColumnCounter == 7 and keyRowCounter == 2:
            drawImg(column7, row2, jHighlightImg)
        if keyColumnCounter == 8 and keyRowCounter == 2:
            drawImg(column8, row2, kHighlightImg)
        if keyColumnCounter == 9 and keyRowCounter == 2:
            drawImg(column9, row2, lHighlightImg)
        if keyColumnCounter == 10 and keyRowCounter == 2:
            drawImg(column10, row2, pHighlightImg)

        if keyColumnCounter == 1 and keyRowCounter == 3:
            drawImg(column1, row3, yHighlightImg)
        if keyColumnCounter == 2 and keyRowCounter == 3:
            drawImg(column2, row3, xHighlightImg)
        if keyColumnCounter == 3 and keyRowCounter == 3:
            drawImg(column3, row3, cHighlightImg)
        if keyColumnCounter == 4 and keyRowCounter == 3:
            drawImg(column4, row3, spaceHighlightImg) 
        if keyColumnCounter == 5 and keyRowCounter == 3:
            drawImg(column4, row3, spaceHighlightImg) 
        if keyColumnCounter == 6 and keyRowCounter == 3:
            drawImg(column4, row3, spaceHighlightImg)                 
        if keyColumnCounter == 7 and keyRowCounter == 3:
            drawImg(column7, row3, vHighlightImg)
        if keyColumnCounter == 8 and keyRowCounter == 3:
            drawImg(column8, row3, bHighlightImg)
        if keyColumnCounter == 9 and keyRowCounter == 3:
            drawImg(column9, row3, nHighlightImg)
        if keyColumnCounter == 10 and keyRowCounter == 3:
            drawImg(column10, row3, mHighlightImg)

        if keyRowCounter == 4:
            drawImg(creditsX, creditsY, ViewTop10HighlightImg)
        if keyRowCounter == 5:
            drawImg(exitX, exitY, BackToMenuHighlightImg)    

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                runningGameOver = False                
                runningMain = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    runningGameOver = False
                if event.key == pygame.K_f:
                    runningHighscore = True
                if event.key == pygame.K_SPACE and keyColumnCounter == 1 and keyRowCounter == 1:
                    nameAdd = "Q"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 2 and keyRowCounter == 1:
                    nameAdd = "W"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 3 and keyRowCounter == 1:
                    nameAdd = "E"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 4 and keyRowCounter == 1:
                    nameAdd = "R"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 5 and keyRowCounter == 1:
                    nameAdd = "T"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 6 and keyRowCounter == 1:
                    nameAdd = "Z"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 7 and keyRowCounter == 1:
                    nameAdd = "U"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 8 and keyRowCounter == 1:
                    nameAdd = "I"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 9 and keyRowCounter == 1:
                    nameAdd = "O"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 10 and keyRowCounter == 1:
                    name = name[:-1]

                if event.key == pygame.K_SPACE and keyColumnCounter == 1 and keyRowCounter == 2:
                    nameAdd = "A"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 2 and keyRowCounter == 2:
                    nameAdd = "S"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 3 and keyRowCounter == 2:
                    nameAdd = "D"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 4 and keyRowCounter == 2:
                    nameAdd = "F"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 5 and keyRowCounter == 2:
                    nameAdd = "G"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 6 and keyRowCounter == 2:
                    nameAdd = "H"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 7 and keyRowCounter == 2:
                    nameAdd = "J"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 8 and keyRowCounter == 2:
                    nameAdd = "K"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 9 and keyRowCounter == 2:
                    nameAdd = "L"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 10 and keyRowCounter == 2:
                    nameAdd = "P"
                    name += nameAdd

                if event.key == pygame.K_SPACE and keyColumnCounter == 1 and keyRowCounter == 3:
                    nameAdd = "Y"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 2 and keyRowCounter == 3:
                    nameAdd = "X"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 3 and keyRowCounter == 3:
                    nameAdd = "C"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 4 and keyRowCounter == 3:
                    nameAdd = " "
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 5 and keyRowCounter == 3:
                    nameAdd = " "
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 6 and keyRowCounter == 3:
                    nameAdd = " "
                    name += nameAdd                                
                if event.key == pygame.K_SPACE and keyColumnCounter == 7 and keyRowCounter == 3:
                    nameAdd = "V"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 8 and keyRowCounter == 3:
                    nameAdd = "B"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 9 and keyRowCounter == 3:
                    nameAdd = "N"
                    name += nameAdd
                if event.key == pygame.K_SPACE and keyColumnCounter == 10 and keyRowCounter == 3:
                    nameAdd = "M"
                    name += nameAdd

                if event.key == pygame.K_SPACE and keyRowCounter == 4:
                    runningHighscore = True
                    runningGameOver = False
                    add_highscore(name, scoreValue)
                if event.key == pygame.K_SPACE and keyRowCounter == 5:
                    runningMenu = True
                    runningGameOver = False
                    add_highscore(name, scoreValue)

                if event.key == pygame.K_LEFT:
                    keyColumnCounter -= keyCounterChange
                if event.key == pygame.K_RIGHT:
                    keyColumnCounter += keyCounterChange
                if event.key == pygame.K_UP:
                    keyRowCounter -= keyCounterChange
                if event.key == pygame.K_DOWN:
                    keyRowCounter += keyCounterChange
                if keyColumnCounter == 5 and keyRowCounter == 3 and event.key == pygame.K_RIGHT:
                    keyColumnCounter = 7
                if keyColumnCounter == 5 and keyRowCounter == 3 and event.key == pygame.K_LEFT:
                    keyColumnCounter = 3

        if keyColumnCounter == 0:
            keyColumnCounter = 1
        if keyColumnCounter == 11:
            keyColumnCounter = 10
        if keyRowCounter == 0:
            keyRowCounter = 1
        if keyRowCounter == 6:
            keyRowCounter = 5
        pygame.display.update()

    while runningHighscore:

        clock.tick(Fps)
        screen.fill((222, 208, 221))
        drawImg(exitX, exitY, BackToMenuHighlightImg)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                runningHighscore = False
                runningMain = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    runningMenu = True
                    runningHighscore = False 

        pygame.draw.line(screen, (0, 0, 0), (150, 80), (150, 84), SCREEN_HEIGHT)

        showHeadline((SCREEN_WIDTH / 2) - 150, 30, "Scoreboard", 37)
        display_highscores()
        pygame.display.update()

    pygame.display.update()